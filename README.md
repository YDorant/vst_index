## Workflow to perform VST index calculation using read depth

#### Contributors: Yann Dorant

#### Correspondence <yann.dorant.1@ulaval.ca>

#### license: Free

## Dependencies - R libraries:

* Dplyr
* Magrittr
* tibble
* foreach
* doParallel
* Rmisc
* ggplot2
* edgeR

## Running workflow:

1. From the vcf file extract read depth values : vcftools --geno-depth

2. Use 01-normalize_gdepth_data.R to normalize the read depth over the dataset

3. Use 02-calcul_Vst_bootstraps.R to perform VST calculations (!set the number of bootstraps line 81)

4. Perform PCA over the normalized read depth