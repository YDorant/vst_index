# Date: June 12 13:41:41 2020
# --------------
# Author: Yann Dorant
# Date: June 12 13:41:41 2020
# Modification:
# --------------
# Libraries
library(dplyr)
library(magrittr)
library(tibble)
library(edgeR)

# --------------

# Set working dir -------------------------
setwd(PATH)

##########################################################################################
######################        Define global functions       ##############################
##########################################################################################

#source my R toolbox functions (check PATH) -------------------------

# load specific useful functions from source
'%ni%' <- Negate('%in%') # reverse of %in%

#---------- Add new project functions -------------------------------

##########################################################################################
###########################        Main script       #####################################
##########################################################################################

# |---------|
# | Step 1  | ================> load data files
# |---------|

#1.load population map
strata <- read.table("01-info_files/population_map.txt", h=T) 
pop.vector <- as.vector(unique(strata$POP)) #create pop vector


#2. load gdepth file of duplicated SNPs (output from vcftools --geno-depth)
gdepth_raw_data <- read.table("02-data/gdepth_example.txt", h=T)
dim(gdepth_raw_data)

# |---------|
# | Step 2  | ================> prepare gdepth data
# |---------|

gdepth_transformed <- dplyr::select(gdepth_raw_data, -POS) %>% #remove POS col
                          dplyr::distinct(., CHROM, .keep_all=TRUE) %>% #remove duplicated loci
                          column_to_rownames(., var = 'CHROM')

# check data
dim(gdepth_transformed)
gdepth_transformed[1:10,1:10]

# |---------|
# | Step 3  | ================> perform normalization
# |---------|
####################
DGE_list <- DGEList(counts=as.matrix(gdepth_transformed)) #create list to store info

gdepth_norm_Fact <-calcNormFactors(DGE_list)

gdepth_normalized <- cpm(gdepth_norm_Fact, normalized.lib.sizes = TRUE, log = F)

gdepth_normalized[1:10,1:10]

#write normalized matrix of CNV read depth
write.table(gdepth_normalized, "02-data/gdepth_normalized_CNVs.txt",
            col.names = T, row.names = T, quote = F, sep="\t")


#make tidy format of gdepth normalized read depths
gdepth.tidy <- reshape2::melt(gdepth_normalized) %>% set_colnames(., c("locus",'IND','gdepth')) %>%
  left_join(., strata, by='IND') %>% select(., IND, POP, locus, gdepth)
head(gdepth.tidy)

#write normalized dataframe of CNV read depth
write.table(gdepth.tidy, "02-data/gdepth_normalized.tidy.txt",
            col.names = T, row.names = F, quote = F, sep="\t")
